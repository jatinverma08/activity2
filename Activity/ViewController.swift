//
//  ViewController.swift
//  Activity
//
//  Created by jatin verma on 2019-06-29.
//  Copyright © 2019 jatin verma. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate  {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }

    
    @IBOutlet weak var messageFromWatch: UILabel!
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    @IBAction func buttonClicked(_ sender: Any) {
        
        
        // check if the watch is paired / accessible
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            let message = ["Message": "Message from Phone"]
            // send the message to the watch
            WCSession.default.sendMessage(message, replyHandler: nil)
        }

    }
    
    // WKSession function used by our app
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
    
       var display = applicationData
        
      
        
        // output a debug message to the terminal
        print("WATCH: Got a message!")
        
        // update the message with a label
    messageFromWatch.text = display
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Check if your IOS version can talk to the phone
        // Does your iPhone support "talking to a watch"?
        // If yes, then create the session
        // If no, then output error message
        if (WCSession.isSupported()) {
            print("PHONE: Phone supports WatchConnectivity!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
            else {
                print("PHONE: Phone does not support WatchConnectivity")
                
                
            }
        

    }


}

