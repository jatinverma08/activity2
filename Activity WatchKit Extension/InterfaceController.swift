//
//  InterfaceController.swift
//  Activity WatchKit Extension
//
//  Created by jatin verma on 2019-06-29.
//  Copyright © 2019 jatin verma. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController , WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    


    @IBOutlet weak var messagefromPhone: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
       
        
        
       
    }
    
    
override func willActivate() {
    // This method is called when watch view controller is about to be visible to user
    super.willActivate()
    
    if WCSession.isSupported() {
        let session = WCSession.default
        session.delegate = self
        session.activate()
    }
    
}


    // WKSession function used by our app
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        // Play a "click" sound when you get the message
        WKInterfaceDevice().play(.click)
        
        // output a debug message to the terminal
        print("WATCH: Got a message!")
        
        // update the message with a label
        messagefromPhone.setText("\(message)")
        
    }
    @IBAction func buttonClicked() {
        print("button pressed watch")
        func sendToPhone(data: [String: Any]) {
           
            var applicationData = ["name": "ViratKohli"]
            
           
            
            if WCSession.isSupported() {
              
                var session = WCSession.default
                session.delegate = self
                session.activate()
                
                if WCSession.default.isReachable {
              
                    
                    session.sendMessage(applicationData, replyHandler: {(_ replyMessage: [String: Any]) -> Void in
                        
                        print("ReplyHandler called = \(replyMessage)")
                        WKInterfaceDevice.current().play(WKHapticType.notification)
                    },
                                        errorHandler: {(_ error: Error) -> Void in
                                            
                                            print("Error = \(error.localizedDescription)")
                    })
                }
            }
        }
        
        
    }
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
